# Gets Exchange Rate, then updates it in Epicor via REST API
# Will only work with 10.2^
#  Jeff Johnson, Angeles Composite Technologies, inc. 1/10/2020
#  jeff.johnson@acti.aero

import requests, json, datetime
from requests.auth import HTTPBasicAuth

#AlphaVantage / Currency Settings:
api_key = ''  #Get a free API key here: https://www.alphavantage.co/support/#
From_Currency = 'USD'
To_Currency = 'MXN'


#Epicor Settings:
Username = "your_username"
Password = "your_password"
AppServerURL = "https://ERPAppServer01/EpicorTest"
EntryPerson = "your_username"

SourceCurrID = "1"
SourceCurrName = "US Dollars"
SourceCurrDesc = "Base Currency"
SourceCurrSymbol = "$"

TargetCurrID = "Peso"
TargetCurrName = "Mexican Peso"
TargetCurrDesc = "Peso"
TargetCurrSymbol = "P$"


#Email Settings:
Email_Host = 'mail.youremailhost.com'
Email_From = 'alerts@youremailhost.com'
Email_To = 'it@youremailhost.com'
Email_CC = 'susan@youremailhost.com]'
Email_Subject = 'Exchange Rate Has Been Updated'
Email_Body = \
           'The exchange rate in Epicor has been updated. ' + From_Currency + ' to ' + To_Currency + ' is now at '



base_url = r"https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE"
main_url = base_url +"&from_currency=" + From_Currency + \
           "&to_currency=" + To_Currency + "&apikey=" + api_key

req_ob = requests.get(main_url) 
result = req_ob.json() 

Exchange_Rate = result["Realtime Currency Exchange Rate"]["5. Exchange Rate"]
EffectiveDate = datetime.datetime.now().isoformat()
CurrentRate = Exchange_Rate
SysDate = EffectiveDate
SysTime = datetime.datetime.now().strftime("%H%M%S")
DisplayRate = CurrentRate


mdata = \
"""{
      "Company": "ACT",
      "SourceCurrCode": "1",
      "EffectiveDate": """ + "\"" + EffectiveDate + "\"" + """,
      "CurrentRate": """ + "\"" + CurrentRate + "\"" + """,
      "SysDate": """ + "\"" + SysDate + "\"" + """,
      "SysTime": """ + "\"" + SysTime + "\"" + """,
      "EntryPerson": """ + "\"" + EntryPerson + "\"" + """,
      "Reference": "",
      "TargetCurrCode": "2",
      "RateGrpCode": "CONV",
      "SysRevID": "0",
      "SysRowID": "00000000-0000-0000-0000-000000000000",
      "DisplayRate": """ + "\"" + DisplayRate + "\"" + """,
      "FixedRate": false,
      "SourceScaleFactor": 1,
      "TargetScaleFactor": 1,
      "HasHistory": false,
      "IsLastEffectiveDate": true,
      "GlbFlag": false,
      "HasSecurity": true,
      "BitFlag": 0,
      "RateGrpCodeDescription": "Conversion",
      "RuleCodeRuleCode": 1,
      "RuleCodeFixedRate": false,
      "RuleCodeDisplayMode": 1,
      "SourceCurrCurrName": """ + "\"" + SourceCurrName + "\"" + """,
      "SourceCurrCurrDesc": """ + "\"" + SourceCurrDesc + "\"" + """,
      "SourceCurrCurrencyID": """ + "\"" + SourceCurrID + "\"" + """,
      "SourceCurrDocumentDesc": "",
      "SourceCurrCurrSymbol": """ + "\"" + SourceCurrSymbol + "\"" + """,
      "SourceCurrMaintRate": true,
      "TargetCurrDocumentDesc": "",
      "TargetCurrCurrSymbol": """ + "\"" + TargetCurrSymbol + "\"" + """,
      "TargetCurrCurrencyID": """ + "\"" + TargetCurrID+ "\"" + """,
      "TargetCurrCurrDesc": """ + "\"" + TargetCurrDesc+ "\"" + """,
      "TargetCurrCurrName": """ + "\"" + TargetCurrName + "\"" + """,
      "TargetCurrMaintRate": true,
      "RowMod": ""
    }"""


headers = {'Content-type' : 'application/json'}
x = requests.post(AppServerURL + '/api/v1/Erp.BO.CurrExRateSvc/CurrExRates', auth=HTTPBasicAuth(Username,Password), verify=False, data = mdata, headers = headers)

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

Email_Body = Email_Body + Exchange_Rate

msg = MIMEMultipart('')

msg['From'] = Email_From
msg['To'] = Email_To
msg['Subject'] = Email_Subject

msg.attach(MIMEText(Email_Body))
s = smtplib.SMTP(host=Email_Host, port=25)
s.send_message(msg)








