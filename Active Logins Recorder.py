#  Will only work on Epicor 10.2+ as it uses the built-in REST API
#
# requires pip install requests
# requires pip install openpyxl
#
#  Jeff Johnson, Angeles Composite Technologies, inc. 1/10/2020
#  jeff.johnson@acti.aero

import json
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import requests
from requests.auth import HTTPBasicAuth
from openpyxl import Workbook
from openpyxl import load_workbook
from datetime import datetime

# Settings
MailHost = 'mail.shimtechgroup.com'

EpicorUserName = 'user'
EpicorPassword = 'pass'
AppServerURL = 'https://erpappserver01/Production/api/v1/'

APIURL = 'Ice.BO.AdminSessionSvc/List'
APIFilter = ''

RequestHeaders = {'Content-type' : 'application/json'}

reqs = requests.get(AppServerURL + APIURL + APIFilter,auth=HTTPBasicAuth(EpicorUserName,EpicorPassword), verify=False, headers = RequestHeaders)

mes = 0
default = 0
other = 0

for y in reqs.json()["value"]:
    if y["InUse"] == True:
        if y["SessionTypeDescription"] == "DefaultUser":
            default += 1
        elif y["SessionTypeDescription"] == "DataCollection":
            mes += 1
        else:
            other += 1
            
    #debug
    print("User: {} : {}".format(y["CurUserID"], y["InUse"]))
print ("MES: {}\nDefault:{}\nOther:{}".format(mes,default,other))

try: 
    wb = load_workbook("epicorloginstats.xlsx", read_only = False)
    
except:
    wb = Workbook()
    wb.save("epicorloginstats.xlsx")
    st = wb.active
    st.cell(row=1, column=1, value='Date')
    st.cell(row=1, column=2, value='MES')
    st.cell(row=1, column=3, value='Default')
    st.cell(row=1, column=4, value='Other')



st = wb.active
max_r = st.max_row

now = datetime.now()

st.cell(row=max_r+1, column=1, value=now.strftime("%d/%m/%Y %H:%M:%S"))
st.cell(row=max_r+1, column=2, value=mes)
st.cell(row=max_r+1, column=3, value=default)
st.cell(row=max_r+1, column=4, value=other)



wb.save("epicorloginstats.xlsx")
 



