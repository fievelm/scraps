#  Looks for pending requisitions awaiting approval and emails requestor and current dispatcher.
#  Users need to have a valid email in User Security Maintenance
#  Will only work on Epicor 10.2+ as it uses the built-in REST API
#
#  Jeff Johnson, Angeles Composite Technologies, inc. 1/30/2020
#  jeff.johnson@acti.aero

import json
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import requests
from requests.auth import HTTPBasicAuth


# Settings
# Change all of these to your environment
MailHost = 'mail.domain.com'
MailFrom = 'user@domain.com'
EpicorUserName = 'epicor'
EpicorPassword = 'password'
AppServerURL = 'https://erpappserver01/Production/api/v1/'


#These shouldn't need changing unless the API changes.
ReqAPIURL = 'Erp.BO.ReqSvc/Reqs?'
ReqAPIFilter = '$filter=StatusType%20eq%20\'P\'%20and%20OpenReq%20eq%20true%20and%20ReqActionID%20eq%20\'APPRREQ\''
UserAPIURL = 'Erp.BO.UserFileSvc/UserFiles?'
UserAPIFilter = '$filter=DcdUserID%20eq%20\'' # append username and escaped quote
RequestHeaders = {'Content-type' : 'application/json'}

#Get Open Reqs
reqs = requests.get(AppServerURL + ReqAPIURL + ReqAPIFilter,auth=HTTPBasicAuth(EpicorUserName,EpicorPassword), verify=False, headers = RequestHeaders)

#Get Req info and email users
for y in reqs.json()["value"]:
    employee = requests.get(AppServerURL + UserAPIURL + UserAPIFilter + y["RequestorID"] + '\'',auth=HTTPBasicAuth(EpicorUserName,EpicorPassword), verify=False, headers = RequestHeaders)
    for a in employee.json()["value"]:
        employee_email = a["EMailAddress"]

    currDispatcher = requests.get(AppServerURL + UserAPIURL + UserAPIFilter + y["CurrDispatcherID"] + '\'',auth=HTTPBasicAuth(EpicorUserName,EpicorPassword), verify=False, headers = RequestHeaders)
    for a in currDispatcher.json()["value"]:
        currDispatcher_email = a["EMailAddress"]

    #debug
    print("Req: {}\nEmployee:{}\nCurrDispatcher:{}".format(y["ReqNum"], employee_email, currDispatcher_email))

    
    msg = MIMEMultipart('')
    body = "{}, you have a req waiting for approval.\n".format(employee.json()["value"][0]['Name'])
    body += "Req#: {}\nDispatcher: {}\nCommentText: {}\nRequestDate: {}".format(
        y['ReqNum'],employee.json()["value"][0]["Name"],y['CommentText'],y['RequestDate'][:-9])
    msg['From'] = MailFrom
    msg['To'] = employee_email 
    msg['Cc'] = currDispatcher_email
    msg['Subject'] = 'You have a Requisition waiting for approval'

    #debug
    print(body)
    
    body +=\
         """

         This message is generated from a python script activated via scheduled task on the Epicor AppServer.

         """
    msg.attach(MIMEText(body))
    s = smtplib.SMTP(host=MailHost,port=25)
    s.send_message(msg)
